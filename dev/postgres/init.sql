CREATE DATABASE eventuality_dev WITH OWNER postgres;
\c eventuality_dev
CREATE EXTENSION "uuid-ossp";
CREATE DATABASE eventuality_tests WITH OWNER postgres;
\c eventuality_tests
CREATE EXTENSION "uuid-ossp";
