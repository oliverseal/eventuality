from django.core.management.base import BaseCommand

from eventuality.users.models import User


class Command(BaseCommand):
    help = (
        'Deletes all users that have an email that matches pattern "test.user.%04d@example.com".\n',
        "Simple Example: \n",
        "    python3 manage.py cleanupusers",
    )

    def handle(self, *args, **options):
        qs = User.objects.filter(email__regex=r"^test\.user\.\d{4}@example\.com$")
        print(f"Cleaning up {qs.count()} users...")
        qs.delete()
        print("Done.")
