from django.core.management.base import BaseCommand

from eventuality.users.factories import UserFactory


class Command(BaseCommand):
    help = (
        "Creates x amount of users.\n",
        "Simple Example: \n",
        "    python3 manage.py createusers 2000",
    )

    def add_arguments(self, parser):
        parser.add_argument("count", nargs=1, type=int)

    def handle(self, *args, **options):
        count = options["count"][0]
        print(f"Creating {count} users...")

        UserFactory.create_batch(count)

        print("Done.")
