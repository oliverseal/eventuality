from django.urls import reverse

import pytest


@pytest.mark.django_db
def test_users_can_login(client, test_password, first_user):
    """
    Just make sure that the login view is working.
    """
    url = reverse("login")
    form_data = {
        "username": first_user.email,
        "password": test_password,
    }

    response = client.post(url, form_data)
    assert response.status_code == 302


@pytest.mark.django_db
def test_users_cannot_login_with_bogus_password(client, test_password, first_user):
    """
    Just make sure that the login isn't letting just everyone in.
    """
    url = reverse("login")
    form_data = {
        "username": first_user.email,
        "password": "this is a bogus password",
    }
    response = client.post(url, form_data)
    response_form = response.context.get("form")
    assert response.status_code == 200
    assert len(response_form.errors) == 1


@pytest.mark.django_db
def test_users_can_register(client, test_password):
    """
    Users can register a new account.
    """
    url = reverse("register")
    form_data = {
        "email": "test.register@example.com",
        "password1": test_password,
        "password2": test_password,
    }
    response = client.post(url, form_data)
    assert response.context is None
    assert response.status_code == 302


@pytest.mark.django_db
def test_users_cannot_register_multiple_times(client, test_password, first_user):
    """
    Users can register an account only once.
    """
    url = reverse("register")
    form_data = {
        "email": first_user.email,
        "password1": test_password,
        "password2": test_password,
    }
    response = client.post(url, form_data)
    assert response.status_code == 200
    response_form = response.context.get("form")
    assert len(response_form.errors) == 1
    # specifically make sure it was the duplicate user bit that was bad
    assert len(response_form.errors["email"]) == 1
