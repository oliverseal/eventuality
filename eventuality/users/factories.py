import factory

from .models import User


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    email = factory.Sequence(lambda n: "test.user.%04d@example.com" % n)
