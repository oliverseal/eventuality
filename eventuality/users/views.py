from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView

from .forms import EventualityUserCreationForm


class RegisterView(CreateView):
    template_name = "registration/register.html"
    form_class = EventualityUserCreationForm

    def get_success_url(self):
        return reverse("events:list")

    def get(self, request):
        # redirect to the main home page if this isn't an anonymous user
        if request.user.is_authenticated:
            return HttpResponseRedirect("/")
        return super().get(request)
