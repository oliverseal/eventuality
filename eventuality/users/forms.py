from django.contrib.auth.forms import UserCreationForm
from django.forms import EmailField

from eventuality.users.models import User


class EventualityUserCreationForm(UserCreationForm):
    # override the meta class because we want all that fun password logic, but we're
    # using email as username

    class Meta:
        model = User
        fields = ("email",)
        field_classes = {"email": EmailField}
