from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models


"""
# User Management

Users register with an email and password. Essentially their username _is_ their email.
"""


class UserManager(BaseUserManager):
    def _create_user(self, username, email, password, **extra_fields):
        """
        Create and save a user with the given email, and password. Many of the builtins pass
        username, so we leave that arg in there, but it's ignored.
        """
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def get_by_natural_key(self, username):
        return super().get_by_natural_key(username)


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        verbose_name = "user"
        verbose_name_plural = "users"
        db_table = "users"

    # django thought of this whole email-is-your-login scenario, so we just hook into it.
    USERNAME_FIELD = "email"
    EMAIL_FIELD = "email"
    objects = UserManager()

    email = models.EmailField("email", max_length=255, unique=True)

    @property
    def public_name(self):
        """
        Since username is overridden to be email, _but_ we don't want things just blasting
        personal email information all over the screens, this will fancy truncate it.
        """
        try:
            return self.email.split("@")[0]
        except Exception:
            # technically not possible, but here in case we have test cases with email-less users
            return "(blank)"
