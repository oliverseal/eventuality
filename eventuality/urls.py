from django.contrib.auth.views import LoginView, LogoutView
from django.urls import include, path
from django.views.generic.base import RedirectView

from eventuality.users.views import RegisterView

urlpatterns = [
    path("", RedirectView.as_view(url="/events/"), name="home"),
    path("events/", include("eventuality.events.urls")),
    path("register/", RegisterView.as_view(), name="register"),
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(template_name="registration/logout.html"), name="logout"),
]
