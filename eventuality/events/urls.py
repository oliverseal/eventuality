from django.urls import path

from .views import (
    AttendEventView,
    CreateEventView,
    DeleteEventView,
    DetailEventView,
    ListEventsView,
    UpdateEventView,
    WithdrawFromEventView,
)

app_name = "events"
urlpatterns = [
    path("", ListEventsView.as_view(), name="list"),
    path("create", CreateEventView.as_view(), name="create"),
    path("<int:event_id>", DetailEventView.as_view(), name="detail"),
    path("<int:event_id>/update", UpdateEventView.as_view(), name="update"),
    path("<int:event_id>/delete", DeleteEventView.as_view(), name="delete"),
    path("<int:event_id>/attend", AttendEventView.as_view(), name="attend"),
    path("<int:event_id>/withdraw", WithdrawFromEventView.as_view(), name="withdraw"),
]
