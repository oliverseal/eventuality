import datetime

from django.forms import ModelForm
from django.forms.fields import SplitDateTimeField
from django.forms.widgets import SplitDateTimeWidget
from django.utils import timezone

from .models import Event


class EventForm(ModelForm):
    """
    The view could handle this fine, but we want to provide some semblance of usability
    with this form, so we'll override the default widget to use date type.
    """

    class Meta:
        model = Event
        fields = ["title", "starts_at", "ends_at", "description"]

    starts_at = SplitDateTimeField(
        initial=lambda: timezone.now(),
        widget=SplitDateTimeWidget(date_attrs={"type": "date"}, time_attrs={"type": "time"},),
    )
    ends_at = SplitDateTimeField(
        initial=lambda: timezone.now() + datetime.timedelta(minutes=30),
        widget=SplitDateTimeWidget(date_attrs={"type": "date"}, time_attrs={"type": "time"},),
    )
