import random
import sys

from django.core.management.base import BaseCommand

from eventuality.events.factories import EventFactory
from eventuality.users.models import User


class Command(BaseCommand):
    help = (
        "Creates x amount of events. Will also auto add random attendees if `--populate` is passed.\n",
        "Simple Example: \n",
        "    python3 manage.py createvents 2000 --populate",
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "--owner",
            "-o",
            type=str,
            help="Required. The email address of the existing user that should own the events.",
        )
        parser.add_argument(
            "--populate",
            "-p",
            help="If set, grabs from the users in the database and adds them in random amounts as attendees.",
            nargs="?",
            default=False,
            const=True,
            type=bool,
        )
        parser.add_argument("count", nargs=1, type=int)

    def handle(self, *args, **options):
        count = options["count"][0]
        print(f"Creating {count} events...")

        owner = User.objects.get(email=options["owner"])

        events = EventFactory.create_batch(count, created_by=owner)
        print("Done")

        if options["populate"]:
            print("Populating events...")
            # max out at 3000 users in the pool
            qs = User.objects.all()[:3000]
            min_count = 10
            # wild idea: potentially we could add every user as an attendee
            max_count = qs.count()

            users = list(qs)

            event_index = 0
            for event in events:
                offset_start = random.randint(min_count, int(max_count / 2))
                user_count = random.randint(min_count, max_count - offset_start)
                random_user_set = random.sample(users[offset_start:], k=user_count)
                event.attendees.add(*random_user_set)
                event.save()
                event_index += 1
                event_progress = int(event_index / count * 100)
                sys.stdout.write(
                    f"\r{event_index} populated with {len(random_user_set)} attendees. ({event_progress}%)      "
                )
                sys.stdout.flush()

            print("\nDone.")
