from django.core.management.base import BaseCommand

from eventuality.events.models import Event


class Command(BaseCommand):
    help = (
        'Deletes all events that have a title that matches pattern "Automated Example Event %04d".\n',
        "Simple Example: \n",
        "    python3 manage.py cleanupevents",
    )

    def handle(self, *args, **options):
        qs = Event.objects.filter(title__regex=r"^Automated Example Event \d{4}$")
        print(f"Cleaning up {qs.count()} events...")
        qs.delete()
        print("Done.")
