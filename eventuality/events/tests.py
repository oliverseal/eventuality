import datetime

from django.urls import reverse

import pytest

from faker import Faker

from eventuality.events.models import Event

gen = Faker()


@pytest.mark.django_db
def test_users_can_see_a_list_of_events(client, first_user, test_password, many_events):
    """
    Logged in users should be see a list of all the events.
    """
    url = reverse("events:list")
    client.login(username=first_user, password=test_password)

    response = client.get(url)
    assert response.status_code == 200
    event = Event.objects.first()
    print("start", event.starts_at)
    print("end", event.ends_at)
    objects = response.context["object_list"]
    # make sure we got back the events from many_events
    assert len(objects) > 0
    # make sure they're in descending order as that's an acceptance criteria
    last_date = objects[0].starts_at
    for event in objects:
        assert event.starts_at <= last_date
        last_date = event.starts_at


@pytest.mark.django_db
def test_anon_users_cannot_see_a_list_of_events(client, first_user, test_password, many_events):
    """
    Anonymous users should be redirected to login.
    """
    url = reverse("events:list")
    login_url = reverse("login")

    response = client.get(url)
    assert response.status_code == 302
    assert response.url.startswith(login_url)


@pytest.mark.django_db
def test_users_can_create_events(client, first_user, test_password):
    """
    Logged in users should be able to create events at will.
    """
    url = reverse("events:create")
    client.login(username=first_user, password=test_password)

    now = datetime.datetime.utcnow()
    event_data = {
        # will generate something like "Look perpendicular on turkey."
        "title": gen.sentence(nb_words=4),
        "starts_at_0": now.strftime("%m/%d/%Y"),
        "starts_at_1": now.strftime("%H:%M:%S"),
        "ends_at_0": (now + datetime.timedelta(minutes=30)).strftime("%m/%d/%Y"),
        "ends_at_1": (now + datetime.timedelta(minutes=30)).strftime("%H:%M:%S"),
    }
    response = client.post(url, event_data)
    assert response.status_code == 302


@pytest.mark.django_db
def test_anon_users_cannot_create_events(client):
    """
    Anonymoususers should not be able to create events at all.
    """
    url = reverse("events:create")
    login_url = reverse("login")

    now = datetime.datetime.utcnow()
    event_data = {
        # will generate something like "Look perpendicular on turkey."
        "title": gen.sentence(nb_words=4),
        "starts_at_0": now.strftime("%m/%d/%Y"),
        "starts_at_1": now.strftime("%H:%M:%S"),
        "ends_at_0": (now + datetime.timedelta(minutes=30)).strftime("%m/%d/%Y"),
        "ends_at_1": (now + datetime.timedelta(minutes=30)).strftime("%H:%M:%S"),
    }
    response = client.post(url, event_data)
    assert response.status_code == 302
    assert response.url.startswith(login_url)


@pytest.mark.django_db
def test_users_can_edit_their_own_events(client, first_user, test_password, first_user_event_1):
    """
    Users should be able to edit their own events.
    """
    url = reverse("events:update", kwargs={"event_id": first_user_event_1.id})
    client.login(username=first_user, password=test_password)

    now = datetime.datetime.utcnow()
    event_data = {
        # will generate something like "Look perpendicular on turkey."
        "title": gen.sentence(nb_words=4),
        "starts_at_0": now.strftime("%m/%d/%Y"),
        "starts_at_1": now.strftime("%H:%M:%S"),
        "ends_at_0": (now + datetime.timedelta(minutes=30)).strftime("%m/%d/%Y"),
        "ends_at_1": (now + datetime.timedelta(minutes=30)).strftime("%H:%M:%S"),
    }
    response = client.post(url, event_data)
    assert response.status_code == 302


@pytest.mark.django_db
def test_users_cannot_edit_other_users_events(client, second_user, test_password, first_user_event_1):
    """
    Users should not be able to edit their another user's events.
    """
    url = reverse("events:update", kwargs={"event_id": first_user_event_1.id})
    client.login(username=second_user, password=test_password)

    now = datetime.datetime.utcnow()
    event_data = {
        # will generate something like "Look perpendicular on turkey."
        "title": gen.sentence(nb_words=4),
        "starts_at_0": now.strftime("%m/%d/%Y"),
        "starts_at_1": now.strftime("%H:%M:%S"),
        "ends_at_0": (now + datetime.timedelta(minutes=30)).strftime("%m/%d/%Y"),
        "ends_at_1": (now + datetime.timedelta(minutes=30)).strftime("%H:%M:%S"),
    }
    response = client.post(url, event_data)
    assert response.status_code == 403
    # also check that the data itself wasn't accidentally altered even though we sent
    # that error code
    event = Event.objects.get(pk=first_user_event_1.id)
    assert event.title != event_data["title"]


@pytest.mark.django_db
def test_users_can_attend_an_event(client, second_user, test_password, first_user_event_1):
    """
    Since events are publicly listed, a logged in user can attend them.
    """
    url = reverse("events:attend", kwargs={"event_id": first_user_event_1.id})
    detail_url = reverse("events:detail", kwargs={"event_id": first_user_event_1.id})

    original_attendees = [attendee.id for attendee in first_user_event_1.attendees.all()]
    assert second_user.id not in original_attendees

    client.login(username=second_user, password=test_password)
    response = client.post(url, None)
    assert response.status_code == 302
    assert response.url == detail_url

    refetched_event = Event.objects.filter(id=first_user_event_1.id).prefetch_related("attendees").first()
    refetched_attendees = [attendee.id for attendee in refetched_event.attendees.all()]

    assert second_user.id in refetched_attendees


@pytest.mark.django_db
def test_users_attending_multiple_times_succeeds(client, second_user, test_password, first_user_event_1):
    """
    Because users might get confused by a race condition or out-of-date state, if they try to attend
    an event they've already attended before, we just return as though they've just attended it.
    """
    url = reverse("events:attend", kwargs={"event_id": first_user_event_1.id})
    detail_url = reverse("events:detail", kwargs={"event_id": first_user_event_1.id})

    original_attendees = [attendee.id for attendee in first_user_event_1.attendees.all()]
    assert second_user.id not in original_attendees

    client.login(username=second_user, password=test_password)
    response = client.post(url, None)
    assert response.status_code == 302
    assert response.url == detail_url

    response = client.post(url, None)
    assert response.status_code == 302
    assert response.url == detail_url

    refetched_event = Event.objects.filter(id=first_user_event_1.id).prefetch_related("attendees").first()
    refetched_attendees = [attendee.id for attendee in refetched_event.attendees.all()]

    assert second_user.id in refetched_attendees
    # make sure that the user wasn't added multiple times
    assert len(list(filter(lambda user_id: user_id == second_user.id, refetched_attendees))) == 1


@pytest.mark.django_db
def test_attending_a_bogus_event_404s(client, second_user, test_password, first_user_event_1):
    """
    While we do want to obfuscate adding a user multiple times to an event, we _do_ actually want to be let them know
    if they've tried to attend a bogus or deleted event. A 404 should let them know the event isn't real.
    """
    url = reverse("events:attend", kwargs={"event_id": 99999999})
    client.login(username=second_user, password=test_password)
    response = client.post(url, None)
    assert response.status_code == 404


@pytest.mark.django_db
def test_users_can_withdraw_from_an_event(client, second_user, test_password, first_user_event_1):
    """
    If they try to remove themselves from an event (even if they weren't attending the event), we let them do so.
    """
    detail_url = reverse("events:detail", kwargs={"event_id": first_user_event_1.id})

    url = reverse("events:attend", kwargs={"event_id": first_user_event_1.id})
    client.login(username=second_user, password=test_password)
    response = client.post(url, None)
    assert response.status_code == 302

    event = Event.objects.filter(id=first_user_event_1.id).prefetch_related("attendees").first()
    original_attendees = [attendee.id for attendee in event.attendees.all()]
    assert second_user.id in original_attendees

    # okay so they are attending the event per above. let's get out of it.

    url = reverse("events:withdraw", kwargs={"event_id": first_user_event_1.id})
    response = client.post(url, None)
    assert response.status_code == 302
    assert response.url == detail_url

    refetched_event = Event.objects.filter(id=first_user_event_1.id).prefetch_related("attendees").first()
    refetched_attendees = [attendee.id for attendee in refetched_event.attendees.all()]

    assert second_user.id not in refetched_attendees


@pytest.mark.django_db
def test_users_can_withdraw_from_an_event_multiple_times(client, second_user, test_password, first_user_event_1):
    """
    If they try to remove themselves from an event multiple times, we let them do so without error, even though the
    second time doesn't really do anything.
    """
    detail_url = reverse("events:detail", kwargs={"event_id": first_user_event_1.id})

    url = reverse("events:attend", kwargs={"event_id": first_user_event_1.id})
    client.login(username=second_user, password=test_password)
    response = client.post(url, None)
    assert response.status_code == 302

    event = Event.objects.filter(id=first_user_event_1.id).prefetch_related("attendees").first()
    original_attendees = [attendee.id for attendee in event.attendees.all()]
    assert second_user.id in original_attendees

    url = reverse("events:withdraw", kwargs={"event_id": first_user_event_1.id})
    response = client.post(url, None)
    assert response.status_code == 302
    assert response.url == detail_url

    refetched_event = Event.objects.filter(id=first_user_event_1.id).prefetch_related("attendees").first()
    refetched_attendees = [attendee.id for attendee in refetched_event.attendees.all()]

    assert second_user.id not in refetched_attendees

    # call it again and expect a 302 event though there's nothing to remove
    url = reverse("events:withdraw", kwargs={"event_id": first_user_event_1.id})
    response = client.post(url, None)
    assert response.status_code == 302
    assert response.url == detail_url


@pytest.mark.django_db
def test_users_can_cancel_thier_own_events(client, first_user, test_password, first_user_event_1):
    """
    Users should be able to delete/cancel their own events.
    """
    url = reverse("events:delete", kwargs={"event_id": first_user_event_1.id})
    client.login(username=first_user, password=test_password)
    response = client.post(url, None)
    assert response.status_code == 302
    assert Event.objects.filter(pk=first_user_event_1.id, canceled_at__isnull=False).exists()


@pytest.mark.django_db
def test_users_cannot_cancel_others_events(client, second_user, test_password, first_user_event_1):
    """
    Users should not be able to cancel events they don't own.
    """
    url = reverse("events:delete", kwargs={"event_id": first_user_event_1.id})
    client.login(username=second_user, password=test_password)
    response = client.post(url, None)
    assert response.status_code == 403
    assert Event.objects.filter(pk=first_user_event_1.id, canceled_at__isnull=True).exists()


@pytest.mark.django_db
def test_users_are_shown_when_an_event_is_canceled(client, first_user, second_user, test_password, first_user_event_1):
    """
    So here's the scenario: a user creates an event, then cancels the event. If a user wants to attend that event, but
    the event has been canceled, we should show them the event has been canceled, and not allow them to attend it.
    """
    url = reverse("events:delete", kwargs={"event_id": first_user_event_1.id})
    client.login(username=first_user, password=test_password)
    response = client.post(url, None)
    assert response.status_code == 302
    assert Event.objects.filter(pk=first_user_event_1.id, canceled_at__isnull=False).exists()

    # event is canceled, now we'll let the second user try to attend it
    url = reverse("events:attend", kwargs={"event_id": first_user_event_1.id})
    detail_url = reverse("events:detail", kwargs={"event_id": first_user_event_1.id})
    client.login(username=second_user, password=test_password)
    response = client.post(url, None)
    # we should get a 302 as usual to the detail
    assert response.status_code == 302
    assert response.url == detail_url

    response = client.get(response.url)
    # but the messages should have an error in it about it being canceled.
    # NOTE: this whole app is making assumptions about the language being English. This specific check will need to be
    #       rewritten in the event of a i18n or l10n feature.
    messages = list(response.context["messages"])
    assert len(messages) > 0
    assert str(messages[0]).find("canceled")
