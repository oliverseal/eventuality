from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator
from django.db import IntegrityError, transaction
from django.db.models import Count, OuterRef, Prefetch, Q, Subquery
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import CreateView, DeleteView, DetailView, ListView, UpdateView, View

from eventuality.users.models import User

from .forms import EventForm
from .models import Event, EventAttendee


class ListEventsView(LoginRequiredMixin, ListView):
    model = Event
    template_name = "events/list.html"
    paginate_by = 20
    ordering = ["starts_at"]

    def get_queryset(self, *args, **kwargs):
        now = timezone.now()
        # so the acceptance criteria is to sort with upcoming events first, this means past events
        # aren't as important, I guess? We'll filter it this way:
        # 1. if an event is currently in progress, we'll show it
        #    (i.e. starts_at is less than now, but ends_at is in the future)
        # 2. if an event is upcoming (starts_at is in the future), we'll show it
        qs = (
            super()
            .get_queryset(*args, **kwargs)
            .filter(Q(starts_at__gt=now) | Q(Q(starts_at__lt=now) & Q(ends_at__gt=now)))
        )

        # we'll return an Event, but we're also interested in how many attendees.
        # let's _not_ make the template layer hit the database to get that and "prefetch" it.

        # When this was initially built, the page would take 5(!) seconds to load if I had
        # events with more than 1000 attendees. I couldn't let that stand.
        # So I threw together a subquery. This gave me the sql I wanted... except for
        # one little "GROUP BY" that django insisted it needed buecase of the annotate.
        attendees_queryset = (
            EventAttendee.objects.filter(
                # use the event id from the url
                event_id=OuterRef("id"),
            )
            .annotate(count=Count("id"))
            .order_by()
            .values("count")
        )
        # so I murdered it. right here.
        attendees_queryset.query.group_by = []

        # get my subquery that does the counts of attendees without creating a left outer join
        subquery = Subquery(attendees_queryset)
        qs = (
            qs.select_related("created_by")
            # uses all that above so that instead of 5 second page load, we have ~60ms
            .annotate(attendee_count=subquery)
        )
        return qs


class DetailEventView(LoginRequiredMixin, DetailView):
    model = Event
    template_name = "events/detail.html"
    pk_url_kwarg = "event_id"
    attendees_queryset = None
    # we're going to paginate the attendees because there could be thousands, per specifications
    paginator = None
    paginate_by = 20

    def get_queryset(self):
        qs = super().get_queryset()
        # pagination on a many to many is interesting in django. if we were doing this in pure
        # sql, we'd just do some subquery stuff magics.... So we have to replicate that thinking
        # in django.
        page_number = self.request.GET.get("page", 1)
        self.attendees_queryset = (
            # subquery the attendees (this won't actually hit the database, but design some sql)
            EventAttendee.objects.filter(
                # use the event id from the url
                event_id=self.kwargs.get(self.pk_url_kwarg)
            ).annotate(count=Count("user_id"))
            # this order by needs to match the users prefetch below in order to provide
            # consistent pagination
            .order_by("user__email")
        )

        self.paginator = Paginator(self.attendees_queryset, self.paginate_by)
        current_page = self.paginator.get_page(page_number)

        # subtract 1 because these are 1-based indexes
        start = current_page.start_index() - 1 if current_page.start_index() > 0 else 0
        end = current_page.end_index()
        print("start", start)
        subquery = Subquery(
            self.attendees_queryset
            # we'll doing a "where user in" type of query so we just need the user id
            .values_list("user_id")
            # limit this based on the paginator page
            # this is where the magical pagination stuff happens that makes this query
            # a little bigger, but faster
            [start:end]
        )

        qs = qs.select_related("created_by").prefetch_related(
            # design our own prefetch, where the users are in the paginated subquery above
            Prefetch("attendees", queryset=User.objects.filter(id__in=subquery).order_by("email"))
        )
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        page_number = self.request.GET.get("page", 1)
        context["page_obj"] = self.paginator.get_page(page_number)
        return context


class CreateEventView(LoginRequiredMixin, CreateView):
    model = Event
    form_class = EventForm
    template_name = "events/create.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class UpdateEventView(LoginRequiredMixin, UpdateView):
    model = Event
    form_class = EventForm
    template_name = "events/update.html"
    pk_url_kwarg = "event_id"

    def get_object(self):
        event = super().get_object()
        if self.request.user != event.created_by:
            raise PermissionDenied
        return event


class DeleteEventView(LoginRequiredMixin, DeleteView):
    model = Event
    pk_url_kwarg = "event_id"

    def get_success_url(self):
        return reverse("events:list")

    def get_object(self):
        event = super().get_object()
        if self.request.user != event.created_by:
            raise PermissionDenied
        return event

    def post(self, request, event_id):
        event = self.get_object()
        event.canceled_at = timezone.now()
        # remove the attendees because the event is canceled
        event.attendees.all().delete()
        event.save()
        print("canceled")
        return HttpResponseRedirect(redirect_to=self.get_success_url())


class AttendEventView(LoginRequiredMixin, View):
    """
    This view breaks away from Django's CRUD view pattern a bit.
    We'll never actually show anything from the join table/model, and querying and
    serializing an event with all its bits is just absurd.

    1. we'll check that it's an actual event. users _do_ need to know if it's not a bogus event.
    2. we just add an entry into the EventAttendee table and redirect to the event detail.

    One thing that's especially unique is that this won't show an integrity error if
    the attendee is already joined. We just redirect them to the event. This, in theory,
    would reduce support calls for a confused user that thinks they got an error when
    trying to join an event they already have (if this were a real app).
    """

    pk_url_kwarg = "event_id"

    def post(self, request, event_id):
        # the logged in user can join any event, but they can never join an event for
        # someone else. So even if they, by some hackery, try to inject a user other
        # than themselves, we'll still join _them_ to the event
        event = Event.objects.filter(pk=event_id).only("canceled_at").first()
        if event is None:
            raise Http404

        if event.canceled_at is not None:
            # this event has been canceled, let the user know
            messages.error(request, "You are unable to attend this event due to it being canceled by the owner.")
            return HttpResponseRedirect(reverse("events:detail", kwargs={"event_id": event_id}))

        try:
            with transaction.atomic():
                try:
                    user = self.request.user
                    attendee = EventAttendee(user=user, event_id=event_id,)
                    attendee.save()
                except IntegrityError:
                    # rollback the transaction and act like nothing happened.
                    # the user is already a part of the event or the event didn't exist
                    transaction.rollback()
        except Exception:
            # if there's another type of error, we want to throw it in messages and still
            # redirect to the detail view for them to see.
            messages.error(request, "There was an error adding you to the event.")

        return HttpResponseRedirect(reverse("events:detail", kwargs={"event_id": event_id}))


class WithdrawFromEventView(LoginRequiredMixin, View):
    """
    This view follows the same pattern as AttendEventView, but with the opposite expectation:
    i.e. users are removed from the event. If they are not a part of the event we fail silently just like
    attending multiple times does.
    """

    pk_url_kwarg = "event_id"

    def post(self, request, event_id):
        # the logged in user can join any event, but they can never join an event for
        # someone else. So even if they, by some hackery, try to inject a user other
        # than themselves, we'll still join _them_ to the event
        exists = Event.objects.filter(pk=event_id).exists()
        if not exists:
            raise Http404

        try:
            with transaction.atomic():
                try:
                    user = self.request.user
                    EventAttendee.objects.filter(user=user, event_id=event_id).delete()
                except IntegrityError:
                    # rollback the transaction and act like nothing happened.
                    # the user is already a part of the event or the event didn't exist
                    transaction.rollback()
        except Exception:
            # if there's another type of error, we want to throw it in messages and still
            # redirect to the detail view for them to see.
            messages.error(request, "There was an error removing you from the event.")

        return HttpResponseRedirect(reverse("events:detail", kwargs={"event_id": event_id}))
