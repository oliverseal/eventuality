from django.db import models
from django.urls import reverse

import humanize

from eventuality.users.models import User
from eventuality.utils.abstract import UpdatableModel


class Event(UpdatableModel):
    class Meta:
        verbose_name = "event"
        verbose_name_plural = "events"
        db_table = "events"

    title = models.CharField("title", max_length=255)
    description = models.TextField("description", null=True, blank=True)
    starts_at = models.DateTimeField("starts", db_index=True)
    ends_at = models.DateTimeField("ends", db_index=True)
    canceled_at = models.DateTimeField("canceled", null=True, blank=True, db_index=True)
    created_by = models.ForeignKey(User, db_index=True, on_delete=models.CASCADE)
    attendees = models.ManyToManyField(to=User, through="events.EventAttendee", related_name="events")

    def get_absolute_url(self):
        return reverse("events:detail", kwargs={"event_id": self.pk})

    @property
    def duration(self):
        return humanize.naturaldelta(self.ends_at - self.starts_at)


class EventAttendee(UpdatableModel):
    """
    The only reason to have a separate table for this many-to-many is to have the `created_at` and `updated_at` fields
    managed by the database. This isn't required by the application, but future us will be happy if we ever want this
    data for any reason, including audit purposes.

    Note that we enforce a `unique_together` on this table, but the views may do some magic to make this invisible to
    the user just for UX purposes.
    """

    class Meta:
        verbose_name = "event attendee"
        verbose_name_plural = "event attendees"
        db_table = "event_attendees"
        unique_together = [
            ["event", "user"],
        ]

    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
