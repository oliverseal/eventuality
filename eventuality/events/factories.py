import datetime
import random

from django.utils import timezone

import factory
import factory.fuzzy

from .models import Event


class EventFactory(factory.DjangoModelFactory):
    """
    Returns an `Event`, not an EventFactory instance when instantiated.
    This factor intentionally does not set the `created_by` value since this is generally so specific to testing.
    """

    class Meta:
        model = Event

    title = factory.Sequence(lambda n: "Automated Example Event %04d" % n)
    description = factory.Faker("text")
    starts_at = factory.fuzzy.FuzzyDateTime(
        start_dt=timezone.now(), end_dt=(timezone.now() + datetime.timedelta(days=1095))
    )
    ends_at = factory.LazyAttribute(lambda e: e.starts_at + datetime.timedelta(minutes=random.randint(30, 90)))
    canceled_at = None
    # autonow_add on these would normally handle this but the factoryboy module is overeager and puts naive dates
    created_at = factory.LazyFunction(datetime.datetime.utcnow)
    updated_at = factory.LazyFunction(datetime.datetime.utcnow)
