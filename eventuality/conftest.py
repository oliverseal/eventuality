from django.utils import timezone

import factory
import factory.fuzzy
import pytest

from .events.factories import EventFactory
from .users.factories import UserFactory


@pytest.fixture
def test_password():
    return "eventuality1"


# Shared User Fixtures


@pytest.fixture
def first_user(test_password):
    user = UserFactory()
    user.set_password(test_password)
    user.save()
    return user


@pytest.fixture
def second_user(test_password):
    user = UserFactory()
    user.set_password(test_password)
    user.save()
    return user


# Shared Event Fixtures


@pytest.fixture
def first_user_event_1(first_user):
    event = EventFactory(created_by=first_user,)
    return event


@pytest.fixture
def first_user_event_with_second_user(first_user, second_user):
    event = EventFactory(created_by=first_user,)
    event.attendees.add(second_user)
    event.save()
    return event


@pytest.fixture
def many_events(first_user, second_user):
    """
    This will generate 100 events with random owners.
    """
    events = EventFactory.create_batch(
        100, created_by=factory.fuzzy.FuzzyChoice([first_user, second_user]), starts_at=timezone.now()
    )
    return events
