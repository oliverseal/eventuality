FROM python:3.8-alpine3.11

ENV APP=/app
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONPATH="${PYTHONPATH}:$APP"
ENV PG_HOSTNAME=$PG_HOSTNAME
ENV PG_DATABASE=$PG_DATABASE
ENV PG_USERNAME=$PG_USERNAME
ENV PG_PASSWORD=$PG_PASSWORD
ENV PG_PORT=$PG_PORT

# local dev this will be a volume defined in docker-compose
WORKDIR $APP

# get the os stuff installed -- generally used for debugging
COPY os-requirements.txt $APP
RUN cat $APP/os-requirements.txt | xargs apk --no-cache add

# get python needs
COPY requirements.txt $APP/requirements.txt
RUN python3 -m pip install -r $APP/requirements.txt

COPY . $APP

# might be overridden by docker-compose, but this is the default
CMD python3 manage.py runserver 0.0.0.0:8000
