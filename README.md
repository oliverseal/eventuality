# Eventuality

A simple event management system written in django.

Users can:

1. Register with an email and password.
2. Login with that same email and password.
3. Create events.
4. Attend events that others have created.
5. Cancel their events.
6. Withdraw from attending others' events.


## Quick Start

Running this expects you have `docker` and `docker-compose`.

1. git pull this thing
2. `docker-compose build`
3. `docker-compose exec eventuality python3 manage.py migrate`
4. `docker-compose up`
5. Click this: http://localhost:8000/


## Preferred Development method

1. Run `dev/watchtests` in a terminal somewhere.
2. Write some tests for a feature.
3. Watch the tests fail.
4. Implement the feature.
5. Watch the tests pass/fail and return to Step 4.
6. Profit.


## Development Notes

- `dev/lint` will lint the files you specify, or the whole directory. Pass `--fix` as the first argument and it will be
  destructive.
- `dev/watchtests` can have `--runonce` passed to it and it will bypass the "watch" aspect of the tests.
- `dev/manage {command}` will run the `manage.py` instructions from inside the docker-compose container. Useful for
  making migrations.


## Architecture Notes

This is a plain-old-django app. No REST API or anything fancy as I didn't want to get mired in designing an API and
potentially clashing with integration testing.


### Where are the templates?

Given the frequent separate between FE and BE teams and their general separation of concerns, I like to have the html
living next to the static files like css and javascript, even though they're used by the Django server. I tend to handle
them separately for the most part so I find it easier to work on when they are grouped in a directory like this.

So look in `/assets/` for the `static` and `templates` folders.


### Volume Testing

Several management commands have been created for the sake of testing large volumns of events and attendees. Suggested use
for testing high volumes:

1. Run `docker-compose up` and register a user at https://localhost:8000/
2. Run `dev/manage createusers 3000`.
   - This should be relatively quick.
   - It will generate 3000 test users.
3. Run `dev/manage createevents 2000 --populate --owner {email}`.
   - Be sure to replace `{email}` with the email you registered in step 1.
   - This will not be as quick. 3 - 5 minutes?
   - Generates events and attendees.
4. Roam around in the app. It should be nice and snappy.


### User Management

The built-in Django user/auth system is Good Enough™. The main deviation is that "email" is used for login and
"username" is completely ignored/removed.


### Infrastructure

_None_ of this is built to do anything other than be run for development. A whole different set of docker files would 
be created for that purpose.


## Testing Philosophy

My general testing philosophy for this app is about "keeping promises to the user". Unit testing is valuable in its
own right, but this app's logic is completely centered around user-interaction. Nearly all of my tests will involve a
Django test client actually calling web urls, rather than doing work with the models. If there were functions of this
app that were not user-interactive (e.g. events have automated updates made to them after a certain amount of time) I
would consider those good candidates for unit tests. My opinion on this is malleable, but this approach has worked well
for me in the past in maintaining a product's integrity and reputation without bogging down development with tests that
candidly can seem very tenuous when a functional test would provide the same coverage as well as ensure user acceptance.

### Getting "Unexpected Duplicate Key" Errors?

For the sake of quick development, the tests in `watchtests` are run with `--keep-db`. This means that sometimes, in
the event of a pretty catastrophic error, there might be data leftover in the database next time the databases run. So
if you start getting "duplicate key value violates" on the fixtures themselves, it's likely this is the case.

Fix it with `dev/watchtests --resetdb` which is made to reset the database before running the tests.
